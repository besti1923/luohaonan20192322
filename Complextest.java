import static org.junit.Assert.*;
import org.junit.Test;
import junit.framework.TestCase;
public class ComplexTest extends TestCase {
    Complex complex = new Complex(1,1);
    @Test
    public void testadd(){
        assertEquals(new Complex(3.3,3.4), complex.ComplexAdd(new Complex(2.3,2.4)));
    }
    //加法
    @Test
    public void testsubtract(){
        assertEquals(new Complex(-5.3,-2.4), complex.ComplexSub(new Complex(6.3,3.4)));
    }
    //减法
    @Test
    public void testmultiply(){
        assertEquals(new Complex(3.0,2.0), complex.ComplexMulti(new Complex(3.0,2.0)));
    }
    //乘法
    @Test
    public void testdivide(){
        assertEquals(new Complex(1.0,1.0), complex.ComplexDiv(new Complex(1.0,1.0)));
        assertEquals(new Complex(0.0,0.0), complex.ComplexDiv(new Complex(1.0,0.0)));
    }
    //边缘
    @Test
    public void testequals(){
        assertEquals(true, complex.equals(new Complex(1.0,1.0)));
    }
    //相等
}
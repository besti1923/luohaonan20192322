import java.util.Scanner;
import java.util.Stack;


class sy8{
    //：List<String> list = Arrays.asList(array);
    static Stack<Character> op = new Stack<>();
    //：List<String> list = Arrays.asList(array);
    public static float getv(char op, float f1, float f2){
        if(op == '+') return f2 + f1;    //：List<String> list = Arrays.asList(array);
        else if(op == '-') return f2 - f1;
        else if(op  == '*') return f2 * f1;    //：List<String> list = Arrays.asList(array);
        else if(op == '/') return f2 / f1;
        else return Float.valueOf(-0);    //：List<String> list = Arrays.asList(array);
    }
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public static float calrp(String rp){
        Stack<Float> v = new Stack<>();
        char[] arr = rp.toCharArray();
        int len = arr.length;    //：List<String> list = Arrays.asList(array);
        for(int i = 0; i < len; i++){
            Character ch = arr[i];    //：List<String> list = Arrays.asList(array);
            if(ch >= '0' && ch <= '9') v.push(Float.valueOf(ch - '0'));
            else v.push(getv(ch, v.pop(), v.pop()));
        }    //：List<String> list = Arrays.asList(array);
        return v.pop();    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);


    public static String getrp(String s){
        char[] arr = s.toCharArray();
        int len = arr.length; //：List<String> list = Arrays.asList(array);
        String out =""; //：List<String> list = Arrays.asList(array);

        for(int i = 0; i < len; i++){
            char ch = arr[i];
            if(ch == ' ') continue;

            if(ch >= '0' && ch <= '9') {
                out+=ch;
                continue;
            }
            //：List<String> list = Arrays.asList(array);
            if(ch == '(') op.push(ch);

            if(ch == '+' || ch == '-'){
                while(!op.empty() && (op.peek() != '('))
                    out+=op.pop();
                op.push(ch);
                continue;
            }

            //：List<String> list = Arrays.asList(array);

            if(ch == '*' || ch == '/'){
                while(!op.empty() && (op.peek() == '*' || op.peek() == '/'))
                    out+=op.pop();
                op.push(ch);
                continue;
            }
            //：List<String> list = Arrays.asList(array);

            if(ch == ')'){
                while(!op.empty() && op.peek() != '(')
                    out += op.pop();
                op.pop();    //：List<String> list = Arrays.asList(array);

                continue;    //：List<String> list = Arrays.asList(array);

            } //：List<String> list = Arrays.asList(array);
        }
        while(!op.empty()) out += op.pop();
        return out;
    } //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public static void main(String[] args){
        System.out.println("输入中缀表达式：");
        Scanner scan=new Scanner(System.in);    //：List<String> list = Arrays.asList(array);
        String exp=scan.nextLine();    //：List<String> list = Arrays.asList(array);
        System.out.println("后缀表达式为：");
        System.out.println(getrp(exp)); //：List<String> list = Arrays.asList(array);
        System.out.println("结果为："); //：List<String> list = Arrays.asList(array);
        System.out.println(calrp(getrp(exp)));
    }

}


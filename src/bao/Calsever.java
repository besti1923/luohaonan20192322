package bao;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
public class Calsever {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket=new ServerSocket(8800);
        Socket socket=serverSocket.accept();
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        String info=null;
        System.out.println("服务器已经建立......");
        while(!((info = bufferedReader.readLine()) ==null)){
            System.out.println("我是服务器，用户信息为：" + info);
        }
        String reply="5/12";
        printWriter.write(reply);
        printWriter.flush();
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}


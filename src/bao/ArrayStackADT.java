package bao;
import java.util.Stack;
public class ArrayStackADT {
    public static void main(String[] args) {
        Stack stack = new Stack();
        stack.push(0);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        // stack.pop();
        System.out.println(stack);
        System.out.println("The top_num is: " + stack.peek());
        System.out.println("The number of Array is: " + stack.size());
        while (!stack.isEmpty()) {
            try {
                System.out.println(stack.pop());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
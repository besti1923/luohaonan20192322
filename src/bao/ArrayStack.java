package bao;
import java.util.Arrays;
public class ArrayStack<T> implements StackADT<T> {
    private int DEFINE_LONG = 100;
    private int top;
    private T[] stack;
    public ArrayStack() {
        top = 0;
        stack = (T[]) (new Object[DEFINE_LONG]);
    }
    public ArrayStack(int initialCapacity) {
        top = 0;
        stack = (T[]) (new Object[initialCapacity]);
    }
    @Override
    public void push(T element) {
        if (size() == stack.length) {
            expandCapacity();
        }
        stack[top] = element;
        top++;
    }
    private void expandCapacity() {
        stack = Arrays.copyOf(stack, stack.length * 2);
    }
    @Override
    public T pop() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("stack");
        }
        top--;
        T temp = stack[top];
        stack[top] = null;
        return temp;
    }
    @Override
    public T peek() {
        if (isEmpty()) {
            throw new EmptyCollectionException("stack");
        }
        return stack[top - 1];
    }
    @Override
    public boolean isEmpty() {
        if (stack[top] == null && top == 0) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public int size() {
        return top;
    }
}
class EmptyCollectionException extends RuntimeException {
    public EmptyCollectionException(String collection) {
        super("The " + collection + " is empty");
    }
}
package bao;
public class Student implements Comparable {
    protected String name;
    protected int number;
    protected String hobby;
    protected Student next = null;

    public Student(String name, int number, String hobby) {
        this.name = name;
        this.number = number;
        this.hobby = hobby;
    }

    @Override
    public int compareTo(Object o) {
        Student temp =(Student) o;
        if (o ==null){
            return 1;
        }else {return  number>temp.number ? 1:-1;
        }
    }}

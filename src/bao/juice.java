package bao;
public abstract class juice extends drink implements Comparable {
    public juice(String name, double price, int number) {
        super(name, price, number);
    }

    public juice() {
    }
    @Override
    public int compareTo(Object o) {
        drink temp = (drink) o;
        if (temp.getPrice() > this.price) {
            return 1;
        } else {
            return 0;
        }
    }
}
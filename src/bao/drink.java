package bao;
public abstract class drink {
    String name;
    double price;
    int number;
    public drink(String name, double price, int number) {
        this.name = name;
        this.price = price;
        this.name = name;
    }
    public drink(){
    }
    public abstract void type();
    public abstract void location();
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public double getPrice()
    {
        return price;
    }
    public void setPrice(double price)
    {
        this.price = price;
    }
    public int getNumber()
    {
        return number;
    }
    public void setNumber(int number)
    {
        this.number = number;
    }
    @Override
    public String toString() {
        return "jrink{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", number=" + number +
                '}';
    }
}
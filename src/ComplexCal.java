import java.util.Scanner;

public class ComplexCal {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a, b, c, d, e;
        //String str ="(2+3i)*(2+3i)=";
        String str = in.nextLine();
        ToCal tocal = new ToCal(str);
        //ToCal tocal =new ToCal("(2+3i)*(2+3i)=");

        tocal.trans();
        a = tocal.getA();
        b = tocal.getB();
        c = tocal.getC();
        d = tocal.getD();
        e = tocal.getE();

        Complex com1=new Complex(a,b);
        Complex com2=new Complex(c,d);
        Complex result=null;

        switch(e){
            case 43:
                result=com1.ComplexAdd(com2);
                break;
            case 42:
                result=com1.ComplexMulti(com2);
                break;
            case 45:
                result=com1.ComplexSub(com2);
                break;
            case 47:
                result=com1.ComplexDiv(com2);
                break;
        }
        System.out.print(str);
        System.out.println(result.toString());

    }

}

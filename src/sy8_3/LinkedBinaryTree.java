package sy8_3;
import com.sun.source.tree.BinaryTree;

import javax.swing.text.Element;
import java.util.Arrays;
import java.util.Iterator;
import java.util.StringTokenizer;

public class LinkedBinaryTree<T> implements BinaryTreeADT<T> {
    protected BinaryTreeNode<T> root;
    protected int modCount;

    public LinkedBinaryTree() {
        root=null;
        modCount=0;
    }

    public LinkedBinaryTree(T element){
        root=new BinaryTreeNode<T>(element);
        modCount=1;
    }

    public LinkedBinaryTree(T element,BinaryTreeNode left,BinaryTreeNode right){
        root=new BinaryTreeNode<T>(element);
        root.setLeft(left);
        root.setRight(right);
        modCount=3;
    }

    @Override
    public T getRootElement() {
        return root.element;
    }

    public BinaryTreeNode<T> getRootNode() {
        return root;
    }

    @Override
    public boolean isEmpty() {
        if(this.size()==0){
            return true;
        }
        else
            return false;
    }

    @Override
    public int size() {
        return modCount;
    }


    @Override
    public boolean contains(T targetsElement) {
        if(find(targetsElement)!=null){
            return true;
        }
        else
            return false;
    }

    @Override
    public T find(T targetElement){
        BinaryTreeNode<T> current =findAgain(targetElement,root);
        if(current==null){
            return null;
        }
        return current.element;
    }

    private BinaryTreeNode<T> findAgain(T targetElement,BinaryTreeNode next){
        if(next==null){
            return null;
        }
        if(next.element.equals(targetElement))
            return next;
        BinaryTreeNode<T> temp=findAgain(targetElement,next.right);
        if(temp==null){
            temp=findAgain(targetElement,next.right);
        }
        return temp;
    }

    public static void preorder(BinaryTreeNode temp){
        System.out.print(temp.element+"\t");
        if(temp.left!=null){
            preorder(temp.left);
        }
        if(temp.right!=null){
            preorder(temp.right);
        }

    }

    public static void postorder(BinaryTreeNode temp){
        if(temp.left!=null){
            postorder(temp.left);
        }
        if(temp.right!=null){
            postorder(temp.right);
        }
        System.out.print(temp.element+"\t");
    }

    public void build(String[] a,String[] b){
        root=LinkedBinaryTree.buildTree(a,b);
        modCount+=a.length;
    }
    //a先序，b中序
    private static BinaryTreeNode buildTree(String[] a, String[] b){
        if(a.length==0||b.length==0)
            return null ;
        BinaryTreeNode node=new BinaryTreeNode(a[0]);
        int num=search(b,a[0]);
        node.setLeft(buildTree(Arrays.copyOfRange(a,1,num+1),Arrays.copyOfRange(b,0,num)));
        node.setRight(buildTree(Arrays.copyOfRange(a,num+1,a.length),Arrays.copyOfRange(b,num+1,b.length)));
        return node;
    }

    public static int search(String[] a,String target){
        for(int i=0;i<a.length;i++){
            if(a[i].equals(target)){
                return i;
            }
        }
        return 0;
    }
}

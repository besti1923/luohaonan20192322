package sy8_3;
import java.util.Scanner;

public class DesionTree {
    private LinkedBinaryTree<String> tree;

    public DesionTree(String[] a,String[] b) {
        tree=new LinkedBinaryTree();
        tree.build(a,b);
    }

    public void evaluate(){
        BinaryTreeNode current=tree.root;
        Scanner in=new Scanner(System.in);
        while (current!=null){
            System.out.println(current.getElement());
            if(in.nextLine().equalsIgnoreCase("n")){
                current=current.getLeft();
            }
            else
                current=current.getRight();
        }
        System.out.println(current.getElement());
    }
}

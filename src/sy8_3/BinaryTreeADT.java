package sy8_3;
import java.util.Iterator;

public interface BinaryTreeADT<T> {
    public T getRootElement();
    public boolean isEmpty();
    public int size();
    public boolean contains(T targetsElement);
    public T find(T targetElement);
    public String toString();
}

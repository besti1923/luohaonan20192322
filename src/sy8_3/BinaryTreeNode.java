package sy8_3;
import com.sun.source.tree.BinaryTree;

public class BinaryTreeNode<T> {
    protected T element;
    protected BinaryTreeNode left,right;

    public BinaryTreeNode(T obj){
        element=obj;
        left=null;
        right=null;
    }

    public BinaryTreeNode(T obj,BinaryTreeNode left,BinaryTreeNode right){
        element=obj;
        if(left==null){
            this.left=null;
        }
        else{
            this.left=left;
        }
        if(right==null){
            this.right=null;
        }
        else
            this.right=right;
    }


    public void setElement(T element) {
        this.element = element;
    }

    public void setLeft(BinaryTreeNode left) {
        this.left = left;
    }

    public void setRight(BinaryTreeNode right) {
        this.right = right;
    }

    public T getElement() {
        return element;
    }

    public BinaryTreeNode getLeft() {
        return left;
    }

    public BinaryTreeNode getRight() {
        return right;
    }

}

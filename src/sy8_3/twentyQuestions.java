package sy8_3;
public class twentyQuestions {
    public static void main(String[] args) {
        System.out.println("-----------经典20问------------");
        System.out.println("----快来猜一猜它究竟是什么呢----");
        System.out.println("-------提示：它是一种食物-------");
        //先序
        String[] a={"它是甜的吗？","它需要烤箱做出来吗？","它是蛋糕吗？","答对了！就是香甜可口的蛋糕！","它是辣味的吗？","它是不是我们的童年回忆？","它是辣条吗？","答对啦！就是辣条~"};
        //中序
        String[] b={"它需要烤箱做出来吗？","它是蛋糕吗？","答对了！就是香甜可口的蛋糕！","它是甜的吗？","它是辣味的吗？","它是不是我们的童年回忆？","它是辣条吗？","答对啦！就是辣条~"};
        DesionTree d=new DesionTree(a,b);
        d.evaluate();
    }
}

package sy8_1;
public class LinearNode<T>{
    private LinearNode<T> next;
    private T element;

    /**    //：List<String> list = Arrays.asList(array);
     * Creates an empty node.
     */
    public LinearNode()
    {
        next = null;
        element = null;
    }    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    /**    //：List<String> list = Arrays.asList(array);
     * Creates a node storing the specified element.
     *    //：List<String> list = Arrays.asList(array);
     * @param elem  the element to stored within the created node
     */
    public LinearNode (T elem)
    {
        next = null;
        element = elem;
    }

    /**
     * Returns the node that follows this one.
     *
     * @return  the node that follows this one
     */
    public LinearNode<T> getNext()
    {
        return next;
    }

    /**
     * Sets the node that follows this one.
     *
     * @param node  the node to be set as the next node for this node
     */
    public void setNext (LinearNode<T> node)
    {    //：List<String> list = Arrays.asList(array);
        next = node;
    }    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public T getElement()
    {
        return element;
    }
    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public void setElement (T elem)
    {
        element = elem;
    }}    //：List<String> list = Arrays.asList(array);

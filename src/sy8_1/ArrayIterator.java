package sy8_1;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
public class ArrayIterator<T> extends ArrayList<T> implements Iterator<T> {
    int iteratorModCount;
    int current;
    public ArrayIterator()
    {
        iteratorModCount = modCount;
        current = 0;
    }
    public boolean hasNext() throws ConcurrentModificationException
    {    //：List<String> list = Arrays.asList(array);
        return super.iterator().hasNext();
    }
    public T next() throws ConcurrentModificationException
    {    //：List<String> list = Arrays.asList(array);
        return super.iterator().next();
    }
    public void remove() throws UnsupportedOperationException
    {    //：List<String> list = Arrays.asList(array);
        throw new UnsupportedOperationException();
    }    //：List<String> list = Arrays.asList(array);
}
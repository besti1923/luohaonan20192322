package sy8_1;


public interface QueueADT<T>
{

    //：List<String> list = Arrays.asList(array);
    public void enqueue (T element);

    public T dequeue() throws EmptyCollectionException;

    /**
     lement in this queue
     */
    public T first() throws EmptyCollectionException;

    //：List<String> list = Arrays.asList(array);
    public boolean isEmpty();//：List<String> list = Arrays.asList(array);
//：List<String> list = Arrays.asList(array);

    public int size();
//：List<String> list = Arrays.asList(array);

    public String toString();
}
package sy8_1;

public class test1 {

    public static void main(String[] args) {
        LinkedBinaryTree<String> current = new LinkedBinaryTree<String>("2");
        BTNode<String> a = current.root;
        a.left = new LinkedBinaryTree<String>("0").root;
        a.right = new LinkedBinaryTree<String>("2").root;
        (a.left).left = new LinkedBinaryTree<String>("1").root;
        (a.left).right = new LinkedBinaryTree<String>("9").root;
        (a.right).left = new LinkedBinaryTree<String>("3").root;
        (a.right).right = new LinkedBinaryTree<String>("2").root;
        ((a.right).left).right = new LinkedBinaryTree<String>("2").root;
        System.out.println("是否为空？");
        System.out.println(current.isEmpty());
        ArrayIterator<String>Iterator = (ArrayIterator<String>) current.preorder();
        System.out.println("先序遍历：");
        for(String i :Iterator){
            System.out.println(i);
        }
        ArrayIterator<String>Iterator2 = (ArrayIterator<String>) current.postorder();
        System.out.println("后序遍历：");
        for(String i :Iterator2){
            System.out.println(i);
        }
        ArrayIterator<String>Iterator3 = (ArrayIterator<String>) current.inorder();
        System.out.println("中序遍历：");
        for(String i :Iterator3){
            System.out.println(i);
        }
    }
}

package sy8_1;
import java.util.Iterator;


public class LinkedBinaryTree<T> implements BinaryTree<T> {
    public BTNode<T> root;//定义根结点
    public BTNode left;
    public BTNode right;

    public LinkedBinaryTree()
    {
        root = null;
    }//未赋值时，根结点为空
    public LinkedBinaryTree (T element)
    {
        root = new BTNode<T>(element);
    }//进行根结点赋值
    public LinkedBinaryTree (T element, LinkedBinaryTree<T> left,
                             LinkedBinaryTree<T> right)//根、左、右、再思考
    {
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }
    public T getRootElement() throws Exception {
        if (root == null)
            throw new Exception ("Get root operation "
                    + "failed. The tree is empty.");
        return root.getElement();
    }
    public LinkedBinaryTree<T> getLeft() throws Exception {
        if (root == null)
            throw new Exception ("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();

        return result;
    }
    public T find (T target) throws Exception {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new Exception("Find operation failed. "
                    + "No such element in tree.");

        return node.getElement();
    }
    public int size()
    {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }
    public Iterator<T> inorder()
    {
        ArrayIterator<T> iter = new ArrayIterator<T>();

        if (root != null)
            root.inorder (iter);

        return  iter;
    }
    public Iterator<T> levelorder() throws EmptyCollectionException {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayIterator<T> iter = new  ArrayIterator<T>();

        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode<T> current = queue.dequeue();

                iter.add (current.getElement());
                //：List<String> list = Arrays.asList(array);    //：List<String> list = Arrays.asList(array);
                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }    //：List<String> list = Arrays.asList(array);
        }    //：List<String> list = Arrays.asList(array);

        return iter;    //：List<String> list = Arrays.asList(array);
    }
    public Iterator<T> ArrayIterator()
    {    //：List<String> list = Arrays.asList(array);
        return inorder();    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
    public LinkedBinaryTree<T> getRight() throws Exception {
        if (root == null)    //：List<String> list = Arrays.asList(array);
            throw new Exception ("Get Right operation "
                    + "failed. The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getRight();
        //：List<String> list = Arrays.asList(array);
        return result;
    }    //：List<String> list = Arrays.asList(array);
    public boolean contains (T target) throws Exception {
        BTNode<T> node = null;    //：List<String> list = Arrays.asList(array);
        boolean result = true;
        if (root != null)    //：List<String> list = Arrays.asList(array);
            node = root.find(target);
        if(node == null)    //：List<String> list = Arrays.asList(array);
            result = false;
        return result;    //：List<String> list = Arrays.asList(array);    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
    public boolean isEmpty() {
        return (root.count()==0);
    }    //：List<String> list = Arrays.asList(array);
    public String toString() {
        ArrayIterator<T> list = (ArrayIterator<T>) preorder();
        String result = "<top of Tree>\n";
        for(T i : list){    //：List<String> list = Arrays.asList(array);
            result += i + "\t";
        }    //：List<String> list = Arrays.asList(array);
        return result + "<bottom of Tree>";
    }    //：List<String> list = Arrays.asList(array);
    public  Iterator<T> preorder() {
        ArrayIterator<T> list = new  ArrayIterator<>();
        //：List<String> list = Arrays.asList(array);
        if(root!=null)
            root.preorder(list);
        return list;    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
    //：List<String> list = Arrays.asList(array);
    public  Iterator<T> postorder() {
        ArrayIterator<T> list = new  ArrayIterator<>();
        //：List<String> list = Arrays.asList(array);
        if(root!=null)
            root.postorder(list);
        return list;    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);


    @Override
    public Iterator<T> iterator() {
        return null;
    }
}

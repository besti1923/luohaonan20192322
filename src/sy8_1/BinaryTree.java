package sy8_1;
import java.util.Iterator;

public interface BinaryTree<T> {
    //  Returns the element stored in the root of the tree.
    public T getRootElement() throws  Exception;

    //  Returns the left subtree of the root.
    public BinaryTree<T> getLeft() throws  Exception;

    //  Return//：List<String> list = Arrays.asList(array);s the right subtree of the root.
    public BinaryTree<T> getRight() throws Exception;


    public boolean contains (T target) throws Exception;
    //：List<String> list = Arrays.asList(array);
    public T find (T target) throws  Exception;
    //：List<String> list = Arrays.asList(array);
    public boolean isEmpty();
    //：List<String> list = Arrays.asList(array);

    public int size();
    //：List<String> list = Arrays.asList(array);
    //  Returns the string representation of the binary tree.
    public String toString();
    //：List<String> list = Arrays.asList(array);
    //  Returns a preorder traversal on the binary tree.
    public Iterator<T> preorder();
    //：List<String> list = Arrays.asList(array);
    public Iterator<T> inorder();
    //：List<String> list = Arrays.asList(array);
    //  Returns a postorder traversal on the binary tree.
    public Iterator<T> postorder();    //：List<String> list = Arrays.asList(array);
    public Iterator<T> levelorder() throws Exception;
    //：List<String> list = Arrays.asList(array);
    Iterator<T> iterator();
}

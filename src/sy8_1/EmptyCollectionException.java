package sy8_1;
public class EmptyCollectionException extends Exception {
    //：List<String> list = Arrays.asList(array);
    public EmptyCollectionException(String queue) {
        System.out.println(queue);    //：List<String> list = Arrays.asList(array);
    }    //：List<String> list = Arrays.asList(array);
}    //：List<String> li
package sy7_1;

public class Searching
{
    public static Comparable linearSearch (int[] arr,int target,int length)
    {
        int i=0;
        int a = target;
        if(length>arr.length)
            return "Abnormal";
        else
        {
            while(arr[i]!=target)
            {
                i++;
                if(i==arr.length)
                    break;
            }
            return i==arr.length?false:true;
        }
    }
}

package Hello;

public class HuffmanNode<T> implements Comparable<HuffmanNode<T>>{
    private  T name;
    private double length;
    private HuffmanNode<T> left;
    private HuffmanNode<T> right;
    String code;

    public HuffmanNode(T name, double length){
        this.name = name;
        this.length = length;
        code = "";
    }

    public T getName() {
        return name;
    }

    public void setName(T name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public HuffmanNode<T> getLeft() {
        return left;
    }

    public void setLeft(HuffmanNode<T> left) {
        this.left = left;
    }

    public HuffmanNode<T> getRight() {
        return right;
    }

    public void setRight(HuffmanNode<T> right) {
        this.right = right;
    }

    public String getCode(){
        return code;
    }

    public void setCode(String str){
        code = str;
    }

    @Override
    public String toString(){
        return "name:"+this.name+";length:"+this.length+";Ϊ: "+this.code;
    }

    @Override
    //ȷλ
    public int compareTo(HuffmanNode<T> other) {
        if(other.getLength() > this.getLength()){
            return 1;
        }
        if(other.getLength() < this.getLength()){
            return -1;
        }
        return 0;
    }
}
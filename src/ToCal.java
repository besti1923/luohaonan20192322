public class ToCal {
    private int a,b,c,d,e;
    private String x;

    public ToCal(String str) {
        this.x=str;
    }
    //(a+bi)*(c+di)=
    public void trans(){
        int i,p;
        for(i=0;x.charAt(i)!='=';i++){
            if(x.charAt(i)>=48&&x.charAt(i)<=57){
                a=x.charAt(i)-48;
                while(x.charAt(i+1)>=48&&x.charAt(i+1)<=57){
                    i++;
                    a=a*10+x.charAt(i)-48;
                }
                if(x.charAt(1)==45)
                    a=-a;
                break;
            }
        }

        p=++i;
        for(i=p+1;x.charAt(i)!='=';i++){
            if(x.charAt(i)>=48&&x.charAt(i)<=57){
                b=x.charAt(i)-48;
                while(x.charAt(i+1)>=48&&x.charAt(i+1)<=57){
                    i++;
                    b=b*10+x.charAt(i)-48;
                }
                if(x.charAt(p)==45)
                    b=-b;
                break;
            }
        }

        e=x.charAt(i+3);
        p=i+5;
        for(i=p;x.charAt(i)!='=';i++){
            if(x.charAt(i)>=48&&x.charAt(i)<=57){
                c=x.charAt(i)-48;
                while(x.charAt(i+1)>=48&&x.charAt(i+1)<=57){
                    i++;
                    c=c*10+x.charAt(i)-48;
                }
                if(x.charAt(p)==45)
                    c = -c;
                break;
            }
        }
        //(2+3i)*(2+3i)=
        p=++i;
        for(i=p+1;x.charAt(i)!='=';i++)
            if (x.charAt(i) >= 48 && x.charAt(i) <= 57) {
                d = x.charAt(i) - 48;
                while (x.charAt(i + 1) >= 48 && x.charAt(i + 1) <= 57) {
                    i++;
                    d = d * 10 + x.charAt(i) - 48;
                }
                if (x.charAt(p) == 45)
                    d = -d;
                break;
            }
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public int getD() {
        return d;
    }

    public int getE() {
        return e;
    }
}

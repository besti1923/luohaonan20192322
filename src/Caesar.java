public class Caesar {
    private String m, c = "";
    private int key;

    public Caesar(String m, int key) {
        this.m = m;
        this.key = key;
    }

    public String trans() {
        for(int i=0;i<m.length();i++){
            char d = m.charAt(i);
            d += key;
            c += d;
        }
        return c;
    }

}
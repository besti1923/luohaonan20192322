package sy7;

import java.util.Arrays;
import java.util.Scanner;

public class Searching {
    public static void main(String[] args) {
        int[] arr = {36, 21, 58, 14, 89, 93, 74, 12, 101, 48, 81};
        Arrays.sort(arr);
        System.out.println("数列排序后为：" + Arrays.toString(arr));
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入你想要查找的数字：");
        int res = scan.nextInt();
        System.out.println("你查询的数字为第：" + BoundarySearching(arr,0, arr.length-1,res) + "位");
    }

    public static int BoundarySearching(int[] arr, int start, int end,int key) {
        if(arr == null) {
            return -1;
        }
        if(start > end) {
            return -1;
        }
        int mid = (end - start)/2 + start;
        if(arr[mid] > key) {
            return BoundarySearching(arr, start, mid-1, key);
        } else if(arr[mid] < key) {
            return BoundarySearching(arr, mid+1, end, key);
        } else {
            return mid;
        }
    }

}


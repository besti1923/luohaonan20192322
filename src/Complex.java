public class Complex{
    private double RealPart;
    private double ImagePart;

    public Complex(double realPart, double imagePart) {
        RealPart = realPart;
        ImagePart = imagePart;
    }
    public static double getRealPart(double r){
        return r;
    }
    public static double getImagePart(double i){
        return i;
    }
    public Complex ComplexAdd(Complex a) {
        return new Complex( RealPart+ a.RealPart, ImagePart + a.ImagePart);
    }
    public Complex ComplexSub(Complex a) {
        return new Complex(RealPart - a.RealPart, ImagePart - a.ImagePart);
    }

    public Complex ComplexMulti(Complex a) {
        return new Complex(RealPart * a.RealPart -  ImagePart* a.ImagePart, RealPart * a.ImagePart + ImagePart * a.RealPart);
    }
    public Complex ComplexDiv(Complex a) {
        return new Complex((RealPart * a.ImagePart + ImagePart * a.RealPart)/(a.ImagePart * a.ImagePart + a.RealPart * a.RealPart), (ImagePart * a.ImagePart + RealPart * a.RealPart)/(a.ImagePart * a.ImagePart + a.RealPart * a.RealPart));
    }
    public boolean equal (Complex obj) {
        return this.equals(obj);
    }
    public String toString() {
        String s="";
        if(ImagePart>0){
            s=RealPart+"+"+ImagePart+"i";
        }
        if(ImagePart==0){
            s=RealPart+"";
        }
        if(ImagePart<0){
            s=RealPart+ImagePart+"i";
        }
        return s;
    }
}

public class ComplexCall {
    private String str;
    private int a, b, c, d, e;
    private Complex result=null;
    public ComplexCall(String stl){
        this.str=stl;
    }
    public void Cal(){
        ToCal tocal =new ToCal(str);
        tocal.trans();
        a = tocal.getA();
        b = tocal.getB();
        c = tocal.getC();
        d = tocal.getD();
        e = tocal.getE();

        Complex com1=new Complex(a,b);
        Complex com2=new Complex(c,d);

        switch(e){
            case 43:
                result=com1.ComplexAdd(com2);
                break;
            case 42:
                result=com1.ComplexMulti(com2);
                break;
            case 45:
                result=com1.ComplexSub(com2);
                break;
            case 47:
                result=com1.ComplexDiv(com2);
                break;
        }
    }

    public String getResult(){
        return result.toString();
    }

}

import org.junit.Test;
import junit.framework.TestCase;
public class myutilTest extends TestCase {
    @Test
    public void testNormal() {
        assertEquals("轻", myutil.percentage2fivegrade(45));
        assertEquals("偏轻", myutil.percentage2fivegrade(55));
        assertEquals("正常", myutil.percentage2fivegrade(65));
        assertEquals("微胖", myutil.percentage2fivegrade(75));
        assertEquals("胖", myutil.percentage2fivegrade(85));
    }
    @Test
    public void testExceptions() {
        assertEquals("错误", myutil.percentage2fivegrade(-55));
        assertEquals("错误", myutil.percentage2fivegrade(505));
    }
    @Test
    public void testBoundary() {
        assertEquals("轻", myutil.percentage2fivegrade(0));
        assertEquals("偏轻", myutil.percentage2fivegrade(50));
        assertEquals("正常", myutil.percentage2fivegrade(60));
        assertEquals("微胖", myutil.percentage2fivegrade(70));
        assertEquals("胖", myutil.percentage2fivegrade(80));
        assertEquals("错误", myutil.percentage2fivegrade(500));
    }
}
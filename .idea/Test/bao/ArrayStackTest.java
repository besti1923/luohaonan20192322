package bao;
import java.util.Stack;
public class ArrayStackTest extends ArrayStack {

    public static void main(String[] args) {
        Stack<java.io.Serializable> stack = new Stack<java.io.Serializable>();
        System.out.println("栈是否为空：" + stack.empty());
        stack.push("2307");
        stack.push("2308");
        stack.push("2312");
        stack.push(2322);
        stack.pop();
        System.out.println("Pop前的栈：" + stack);
        stack.pop();
        stack.pop();
        stack.push("2301");
        stack.pop();
        System.out.println("Pop后的栈：" + stack);
        if (!stack.empty()) {
            stack.pop();
        }
        System.out.println("Pop后的栈——Bei：" + stack);

        stack.push("2303");
        stack.push("2307");
        stack.push("2308");
        stack.push("2312");
        System.out.println("Peek；" + stack.peek());
        System.out.println("Peek后的栈：" + stack);
        System.out.println("Size；" + stack.size());
        System.out.println("isEmpty；" + stack.isEmpty());

    }
}
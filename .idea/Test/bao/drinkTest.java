package bao;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
public class drinkTest {
    public static void main(String[] args) {
        juice drink1 = new juice("西瓜汁", 19.9, 0) {
            @Override
            public void type() {

            }

            @Override
            public void location() {

            }
        };
        juice drink2 = new juice() {
            @Override
            public void type() {

            }

            @Override
            public void location() {

            }
        };

        System.out.println("输入第二种果汁名字：");

        BufferedReader buf;
        buf = new BufferedReader(new InputStreamReader(System.in));
        String str1 = null;
        try {
            str1 = buf.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        drink2.setName(str1);

        System.out.println("请输入第二种果汁的价格：");
        Scanner reader = new Scanner(System.in);
        double dou1 = reader.nextDouble();
        drink2.setPrice(dou1);

        System.out.println("请输入第二种果汁的编号：");
        int num1;
        num1 = reader.nextInt();
        drink2.setNumber(num1);

        if (drink1.compareTo(drink2) > 0) {
            System.out.println(drink2.getName() + "价格更高");
        } else {
            System.out.println(drink1.getName() + "价格更高");
        }

        drink1.type();
        drink1.location();
        System.out.println(drink1.toString());
        System.out.println(drink2.toString());
        System.out.println("西瓜汁的哈希值：" + drink1.hashCode());

    }

}
public class complex1 {
    double RealPart;
    double ImagePart;
    public Complex(){
        RealPart = 0;
        ImagePart = 1;
    }
    public complex1(double R,double I){
        ImagePart = I;
        RealPart = R;
    }
    public boolean equals(Object obj){
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Complex1)) {
            return false;
        }
        Complex complex = (Complex) obj;
        if(complex.RealPart != ((Complex) obj).RealPart) {
            return false;
        }
        if(complex.ImagePart != ((Complex) obj).ImagePart) {
            return false;
        }

        return true;
    }
    public String toString()   {
        String string = "";
        if (ImagePart > 0)
            string =  RealPart + "+" + ImagePart + "i";
        if (ImagePart == 0)
            string =  RealPart + "";
        if (ImagePart < 0)
            string = RealPart + " " + ImagePart + "i";
        return string;
    }
    Complex ComplexAdd(Complex a) {
        return  new Complex(RealPart+a.RealPart,ImagePart+a.ImagePart);
    }
    Complex ComplexSub(Complex a) {
        return new Complex(RealPart-a.RealPart,ImagePart-a.ImagePart);
    }
    Complex ComplexMulti(Complex a) {
        return new Complex(RealPart*a.RealPart,ImagePart*a.ImagePart);
    }
    Complex ComplexDiv(Complex a) {
        if(a.RealPart==0||a.ImagePart==0) {
            System.out.println("被减数不能为0");
            return new Complex();
        }

        double d = Math.sqrt(a.RealPart*a.RealPart)+Math.sqrt(a.ImagePart*a.ImagePart);
        return new Complex((RealPart*a.RealPart+ImagePart*a.ImagePart)/d,Math.round((RealPart*a.ImagePart-ImagePart*a.RealPart)/d));
    }
}